All the nodes are on the same network.

    ping server
    ping node1
    ping node2

Ansible uses SSH for connecting to clients.
The access is ensured via SSH keys.

    ssh node1
