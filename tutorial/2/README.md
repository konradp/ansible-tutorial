# Example 2

This example Ansible playbook covers:
- running playbook against hosts (other than localhost)
- inventory (i.e. the `hosts` file)
- Ansible config file
- Ansible facts

Ansible gathers facts about the nodes. These facts can be used as variables in the playbooks.

Print all facts of all nodes.

    ansible all -m setup

print all facts for debian nodes (node1 really). See especially the `ansible_distribution* facts.

    ansible debian -m setup
