# Ansible tutorial
This Ansible tutorial uses Docker Compose to provision an Ansible server and Ansible clients inside Docker containers. See the [blog post](https://konradp.gitlab.io/blog/post/2018-10-20-ansible-tutorial).

# Prerequisites
Install Docker Compose.

    sudo apt-get install docker-compose

Add aliases. Add these e.g. to your `~/.bashrc` file.

    alias d='docker'
    alias dc='docker-compose'

# Docker Compose: Run, rebuild, stop
Start the app in detached mode, show running Docker containers.

    dc build && dc up -d && d ps

Rebuild and start the app.

    dc stop && dc build && dc up -d

Stop the app.

    dc stop

# Start tutorial

After starting the app, connect to the containers.

    d exec -it server bash
    d exec -it node1 bash
    d exec -it node1 bash

Follow the tutorial in the `/opt/tutorial` directory on the `server` container.

See https://stackoverflow.com/questions/39350104/testing-ansible-playbook-with-systemd-services-in-docker  
for systemd in docker hack
