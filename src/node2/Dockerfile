FROM centos:latest

# A hack for Dockerfile with systemd
COPY docker-systemd.sh /tmp/
RUN /tmp/docker-systemd.sh
VOLUME [ "/sys/fs/cgroup", "/run", "/tmp" ]

# Install prerequisite packages
RUN yum -y update; yum -y clean all
RUN yum -y install openssh-server python3 sudo

# Add user: variables
ARG USR=ansible
ARG UID=1000
ARG GID=1000
ARG HOM=/home/$USR

# Add user
RUN groupadd -g $GID $USR
RUN useradd -m \
  -u $UID \
  -g $GID \
  -s /bin/bash \
  -G wheel \
  $USR

# Allow user to sudo with no password
RUN echo "$USR ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/$USR
RUN chmod 0440 /etc/sudoers.d/$USR

# Prepare SSH
RUN mkdir $HOM/.ssh
COPY authorized_keys $HOM/.ssh/
RUN chown -R $USR:$USR $HOM/.ssh
RUN mkdir /run/sshd
RUN /usr/bin/ssh-keygen -A
RUN systemctl enable sshd.service
EXPOSE 22

CMD ["/usr/sbin/init"]
